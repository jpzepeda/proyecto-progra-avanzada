﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using SchoolMVC.Models;
using Packt.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using System.Security.Cryptography;
using System.Security.Claims;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Text;

namespace SchoolMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private School db;

        public HomeController(ILogger<HomeController> logger,School injectedContext)
        {
            _logger = logger;
            db = injectedContext;
        }

        public IActionResult Index()
        {
        IEnumerable<string> Departaments = db.Departaments.Select(d => d.DepartamentName);
        ViewBag.Message = Departaments;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [Authorize]
        public IActionResult Secured()
        {
          var Employee = db.Employees
            .Include(e => e.Campus)
            .Include(e => e.Departament)
            .Include(e => e.EmployeeType)
            .Where(e => e.Email == User.FindFirst(ClaimTypes.Email).Value).FirstOrDefault();
          _logger.LogInformation("Employee first last name is {employeeLastName1}",Employee.LastName1);
          ViewData["Img"] = Convert.ToBase64String(Employee.Img);
          ViewData["BD"] = Employee.BirthDate.ToString();
          ViewData["ED"] = Employee.EntryDate.ToString();
          ViewData["campus"] =  Employee.Campus.CampusName;
          ViewData["departament"] = Employee.Departament.DepartamentName;
          ViewData["employeeType"] = Employee.EmployeeType.Description;
          return View();
        }

        [Authorize]
        public IActionResult Permissions()
        {
          var Employee = db.Employees
            .Include(e => e.Campus)
            .Include(e => e.Departament)
            .Include(e => e.EmployeeType)
            .Where(e => e.Email == User.FindFirst(ClaimTypes.Email).Value).FirstOrDefault();

          var Permissions = db.Permissions
            .Include(p => p.Employee)
            .Include(p => p.PermissionType)
            .Where(p => p.EmployeeId == Employee.EmployeeID);
            ViewBag.Permissions = Permissions;
            foreach(var permission in Permissions)
            _logger.LogInformation("Permission {employeeLastName1}",permission.PermissionId);
          return View();
        }

        [Authorize]
        public async Task<IActionResult> LogOut()
        {
          await HttpContext.SignOutAsync();
          return Redirect("/");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

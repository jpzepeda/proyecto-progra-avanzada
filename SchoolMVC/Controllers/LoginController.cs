﻿  using System;
  using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
  using System.Diagnostics;
  using System.Linq;
  using System.Threading.Tasks;
  using Microsoft.AspNetCore.Mvc;
  using Microsoft.AspNetCore.Http;
  using Microsoft.Extensions.Logging;
  using SchoolMVC.Models;
  using Packt.Shared;
  using Microsoft.AspNetCore.Authorization;
  using Microsoft.AspNetCore.Authentication.Cookies;
  using Microsoft.AspNetCore.Authentication;
  using System.Security.Cryptography;
  using System.Security.Claims;
  using Microsoft.AspNetCore.Cryptography.KeyDerivation;
  using System.Text;

  namespace SchoolMVC.Controllers
  {
      public class LoginController : Controller
      {
          private readonly ILogger<LoginController> _logger;

          private School db;

          public LoginController(ILogger<LoginController> logger,School injectedContext)
          {
              _logger = logger;
              db = injectedContext;
          }


          [HttpPost("Login")]
          public async Task<IActionResult> ValidateCredentials(string username, string password, string returnUrl)
          {
            //TODO: Add Client side JS Hashing to at lest prevent that a MITM Atack able to trasspass SSL could acces other sites that use the same credentials
            var Employee = db.Employees.Where(e => e.Email == username).Include(e => e.Campus).Include(e => e.Departament).FirstOrDefault();
            ViewData["ReturnUrl"] = returnUrl;

            if(Employee != null)
            {
              _logger.LogInformation("Employee first last name is {employeeLastName1}",Employee.LastName1);

              byte[] salt = Convert.FromBase64String(Employee.Salt);

              string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                    password: password,
                    salt: salt,
                    prf: KeyDerivationPrf.HMACSHA1,
                    iterationCount: 10000,
                    numBytesRequested: 256 / 8));
              _logger.LogInformation("The computed hash is {hash}",hashed);
              _logger.LogInformation("The stored hash is {hash}",Employee.Pass);
              //Comparing the hashes
            if (hashed == Employee.Pass)
            {
              _logger.LogInformation("Correct Password");
              //Preparing list of claims for the user
              var claims = new List<Claim>();
              claims.Add(new Claim(ClaimTypes.Email,Employee.Email));
              claims.Add(new Claim("ID",Employee.Email));
              claims.Add(new Claim(ClaimTypes.Role,Employee.EmployeeTypeId.ToString()));
              claims.Add(new Claim(ClaimTypes.Name,Employee.Name));
              claims.Add(new Claim("Campus",Employee.Campus.ToString()));
              claims.Add(new Claim("Departament",Employee.Departament.ToString()));
              claims.Add(new Claim(ClaimTypes.Surname,Employee.LastName1 + " " + Employee.LastName2));
              /* claims.Add(new Claim(ClaimTypes.DateOfBirth,Employee.BirthDate)); */
              //Storing credentials, creatting a claims identity and then a claimsPrincipal, finaly adding it
              var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
              var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
              await HttpContext.SignInAsync(claimsPrincipal);
              if(!string.IsNullOrEmpty(returnUrl))
              return Redirect(returnUrl);
              else
              return Redirect("/");
            }
            else
            {
              _logger.LogInformation("Incorrect Password");
              TempData["Error"] = "User Name or Password Is invalid";
              return Redirect("/login");
            }
            } else
            {
              _logger.LogInformation("Employee not founded");
              TempData["Error"] = "User Name or PassWord Is invalid";
              return Redirect("/login");
            }
          }

          [HttpGet("Login")]
          public IActionResult Index(string returnUrl)
          {
              ViewData["ReturnUrl"] = returnUrl;
              return View();
          }
    }
}

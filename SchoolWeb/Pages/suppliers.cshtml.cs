using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;
using Packt.Shared;
using Microsoft.AspNetCore.Mvc;

namespace SchoolWeb.Pages
{
    public class EmployeesModel : PageModel
    {
        public IEnumerable<string> Departaments { get; set; }
        [BindProperty]
        public Departament Departament {get; set;}
        private School db;
        public void OnGet()
        {
            ViewData["Title"] = "School Website - Departaments";
            Departaments = db.Departaments.Select(d => d.DepartamentName);
        }

        public EmployeesModel(School injectedContext)
        {
            db = injectedContext;
        }

        public IActionResult OnPost()
        {
            if(ModelState.IsValid)
            {
                db.Departaments.Add(Departament);
                db.SaveChanges();
                return RedirectToPage("/suppliers");
            }
            return Page();
        }

    }
}

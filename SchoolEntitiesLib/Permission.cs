using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using static System.Console;

#nullable disable

namespace Packt.Shared
{
  [Table("permisos")]
  public partial class Permission
  {
    [Key]
    [Column("id_permiso",TypeName = "int")]
    [Required]
    public long PermissionId { get; set; }

    [Column("id_empleado",TypeName = "int")]
    [Required]
    public long EmployeeId { get; set; }

    [Column("id_tipo_permiso",TypeName = "int")]
    [Required]
    public long PermissionTypeId { get; set; }

    [Column("fecha_peticion",TypeName = "date")]
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTime AskDate { get; set; }

    [Column("fecha_permiso",TypeName = "date")]
    [Required]
    public DateTime PermissionDate { get; set; }

    #nullable enable
    [Column("motivo",TypeName = "nvarchar (255)")]
    public String? Motive { get; set; }

    [Column("numero_dias",TypeName = "int")]
    public long? NumberOfDays { get; set; }

    #nullable disable
    [Column("aprobado",TypeName = "int")]
    public long Aproved { get; set; }

    [ForeignKey(nameof(EmployeeId))]
    [InverseProperty("Permissions")]
    public virtual Employee Employee { get; set; }

    [ForeignKey(nameof(PermissionTypeId))]
    [InverseProperty("Permissions")]
    public virtual PermissionType PermissionType { get; set; }

    //Mustn include the virtual fields otherwise it'll fail (Including recursive those in Employee)
    public static bool CheckPermision(Permission permission,DbSet<Permission> permissions)
    //NOTE: permission is not necesarly included in the BDSet that's why is also passed and not just the PK
    {
      if(permission.Employee == null ||
          permission.PermissionType == null ||
          permission.Employee.EmployeeType == null)
        //virtual fields not included
        return false;
      else
      {
        if(permission.Employee.EmployeeType.Description == "Eventual")
          //Eventual employees can't ask for permissions
          return false;
        else
        {
          //2 hours permission (7:00 a 9:00 ó de 13:00 a 15:00)
          if(permission.PermissionType.Description == "Por 2 horas")
          {
            var permissionTime = permission.PermissionDate;
            var extracted = new DateTime(2000,1,1,permissionTime.Hour,permissionTime.Minute,0);
            WriteLine(extracted.ToString());
            var begin1 = new DateTime(2000,1,1,7,0,0);
            var end1 = new DateTime(2000,1,1,9,0,0);
            var begin2 = new DateTime(2000,1,1,13,0,0);
            var end2 = new DateTime(2000,1,1,15,0,0);
            //420
            /* if(!(DateTime.Compare(extracted,begin1) >= 0 && DateTime.Compare(extracted,end1) <= 0 || */
            /*     DateTime.Compare(extracted,begin2) >= 0 && DateTime.Compare(extracted,end2) <= 0)) */
            if(!(DateTime.Compare(extracted,begin1) == 0 ||
                DateTime.Compare(extracted,begin2) == 0 ))
              //Doesn't correspont to correct time
              return false;
            else
            {
              //Extract day to compute quincena (as no known definition has been provided, defining frist quincena has 15 days and the second varies depending on the year/month)
              var extractedd = new DateTime(2000,1,permissionTime.Day,0,0,0);
              var begin1d = new DateTime(2000,1,1,0,0,0);
              var end1d = new DateTime(2000,1,15,0,0,0);
              var begin2d = new DateTime(2000,1,16,0,0,0);
              var end2d = new DateTime(2000,1,31,0,0,0);
              if(DateTime.Compare(extractedd,begin1d) >= 0 && DateTime.Compare(extractedd,end1d) <= 0)
              {
                //First
                var beginQuincena = new DateTime(permissionTime.Year,permissionTime.Month,1,0,0,0);
                var endQuincena = new DateTime(permissionTime.Year,permissionTime.Month,15,0,0,0);
                //Queryng the number of permissions in the quincena
                //Note: You may get rid of the {} in the lambda
                var permissionsInQuincena = permissions.Where(p =>
                    DateTime.Compare(p.PermissionDate,beginQuincena) >= 0 &&
                    DateTime.Compare(p.PermissionDate,endQuincena) <= 0 &&
                    p.EmployeeId == permission.EmployeeId &&
                    p.PermissionTypeId == 1
                    ).Count();
                    WriteLine("Primera quincena con {0} permissions",permissionsInQuincena.ToString());
                    if(permissionsInQuincena >= 2)
                      //reached limit for this kind of permission
                      return false;
                    else
                      return true;
              } else
              {
                //Second
                var beginQuincena = new DateTime(permissionTime.Year,permissionTime.Month,16,0,0,0);
                var endQuincena = new DateTime(permissionTime.Year,permissionTime.Month,
                    DateTime.DaysInMonth(permissionTime.Year,permissionTime.Month),0,0,0);
                var permissionsInQuincena = permissions.Where(p =>
                    DateTime.Compare(p.PermissionDate,beginQuincena) >= 0 &&
                    DateTime.Compare(p.PermissionDate,endQuincena) <= 0 &&
                    p.EmployeeId == permission.EmployeeId &&
                    p.PermissionTypeId == 1
                    ).Count();
                    WriteLine("Segunda quincena con {0} permissions",permissionsInQuincena.ToString());
                    if(permissionsInQuincena >= 2)
                      //reached limit for this kind of permission
                      return false;
                    else
                      return true;
              }
            }
          }
          //birthday permission
          else if(permission.PermissionType.Description =="Cumpleaños")
          {
              return true;
          }
          //economic days permission
          else if(permission.PermissionType.Description == "Dias economicos")
          {
              if(!permission.Employee.EntryDate.HasValue)
                //No entry Date (Just for the sake of code sanity)
                return false;
              else {
                if(!(DateTime.Compare(permission.PermissionDate,permission.Employee.EntryDate.Value.AddYears(1)) >= 0))
                  //Less than a year working
                  return false;
                else
                {
                  if(!permission.NumberOfDays.HasValue)
                    //Empty Number of days (null)
                    return false;
                  else
                  {
                    if(!(permission.NumberOfDays.Value <= 3 && permission.NumberOfDays.Value >= 1))
                      //Number of days invalid
                      return false;
                    else
                      return true;
                  }
                }
              }
          }
          else
          {
              return true;
          }
        }
      }
    }
  }
}

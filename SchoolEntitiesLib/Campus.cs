using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;


namespace Packt.Shared
{
  [Table("planteles")]
  public partial class Campus
  {
    public Campus()
    {
      Employees = new HashSet<Employee>();
    }

    [Key]
    [Column("id_plantel",TypeName = "int")]
    [Required]
    public long CampusId { get; set; }

    [Column("nombre_plantel",TypeName = "nvarchar (45)")]
    [StringLength(45)]
    [Required]
    public string CampusName { get; set; }

    #nullable enable
    [Column("director",TypeName = "int")]
    public long? DirectorId { get; set; }

    [ForeignKey(nameof(DirectorId))]
    public virtual Employee? Director { get; set; }

    #nullable disable
    [InverseProperty(nameof(Employee.Campus))]
    public virtual ICollection<Employee> Employees { get; set; }

  }
}

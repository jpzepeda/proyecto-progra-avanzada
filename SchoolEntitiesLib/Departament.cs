using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Packt.Shared
{
  [Table("departamentos")]
  public partial class Departament
  {
    public Departament()
    {
      Employees = new HashSet<Employee>();
    }

    [Key]
    [Column("id_departamento",TypeName = "int")]
    [Required]
    public long DepartamentId { get; set; }

    [Column("nombre_departamento",TypeName = "nvarchar (60)")]
    [StringLength(33)]
    [Required]
    public string DepartamentName { get; set; }

    #nullable enable
    [Column("jefe",TypeName = "int")]
    public long? BoosId { get; set; }

    [ForeignKey(nameof(BoosId))]
    public virtual Employee? Boss { get; set; }

    #nullable disable
    [InverseProperty(nameof(Employee.Departament))]
    public virtual ICollection<Employee> Employees { get; set; }

  }
}

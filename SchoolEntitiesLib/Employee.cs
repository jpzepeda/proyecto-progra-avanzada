﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Packt.Shared
{

  [Table("empleados")]
  public partial class Employee
  {
    public Employee()
    {
      Permissions = new HashSet<Permission>();
    }

    [Key]
    [Column("id_empleado",TypeName = "int")]
    [Required]
    public long EmployeeID { get; set; }

    [Column("nombre",TypeName = "nchar (45)")]
    [StringLength(45)]
    [Required]
    public string Name { get; set; }

    [Column("apellido_paterno",TypeName = "nvarchar (45)")]
    [StringLength(45)]
    [Required]
    public string LastName1 { get; set; }

    [Column("email",TypeName = "nvarchar (60)")]
    [StringLength(60)]
    [Required]
    public string Email { get; set; }

    [Column("apellido_materno",TypeName = "nvarchar (45)")]
    [StringLength(45)]
    public string LastName2 { get; set; }

    [Column("pass",TypeName = "nvarchar (255)")]
    [StringLength(255)]
    [Required]
    public string Pass { get; set; }

    [Column("salt",TypeName = "nvarchar (255)")]
    [StringLength(255)]
    [Required]
    public string Salt { get; set; }

    [Column("fecha_nacimiento",TypeName = "date")]
    [Required]
    public DateTime BirthDate { get; set; }

    [Column("fecha_ingreso",TypeName = "date")]
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTime? EntryDate { get; set; }

    [Column("id_departamento",TypeName = "int")]
    [Required]
    public long DepartamentId { get; set; }

    [Column("id_plantel",TypeName = "int")]
    [Required]
    public long CampusId { get; set; }

    [Column("id_tipo_empleado",TypeName = "int")]
    [Required]
    public long EmployeeTypeId { get; set; }

    [Column("img",TypeName = "image")]
    public byte[] Img { get; set; }

    [ForeignKey(nameof(DepartamentId))]
    public virtual Departament Departament { get; set; }

    [ForeignKey(nameof(CampusId))]
    public virtual Campus Campus { get; set; }

    [ForeignKey(nameof(EmployeeTypeId))]
    public virtual EmployeeType EmployeeType { get; set; }

    [InverseProperty(nameof(Permission.Employee))]
    public virtual ICollection<Permission> Permissions { get; set; }
  }
}

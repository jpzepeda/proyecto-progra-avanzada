using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Packt.Shared
{
  [Table("invitations")]
  public partial class Invitation
  {
    [Key]
    [Column("id_invitations",TypeName = "int")]
    [Required]
    public long InvitationId { get; set; }

    [Column("email",TypeName = "text")]
    [Required]
    public string Email { get; set; }

    [Column("code",TypeName = "text")]
    [Required]
    public string Code { get; set; }

    [Column("used",TypeName = "integer")]
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public long Used { get; set; }
  }
}

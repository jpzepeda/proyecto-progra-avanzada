using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Packt.Shared
{
  [Table("calendario_sep")]
  public partial class Calendar
  {

    [Column("FreeDayId",TypeName = "int")]
    [Key]
    [Required]
    public long FreeDayId { get; set; }

    [Column("fecha_inicio", TypeName = "date")]
    [Required]
    public DateTime CustomerID { get; set; }

    [Column("numero_dias", TypeName = "int")]
    public long? FreeDays { get; set; }
  }
}

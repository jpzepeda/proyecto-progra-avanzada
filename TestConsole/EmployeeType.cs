using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Packt.Shared
{
  [Table("tipo_empleado")]
  public partial class EmployeeType
  {
    public EmployeeType()
    {
      Employees = new HashSet<Employee>();
    }

    [Key]
    [Column("id_tipo_empleado",TypeName = "int")]
    [Required]
    public long EmployeeTypeId { get; set; }

    [Column("descripcion",TypeName = "nvarchar (45)")]
    [StringLength(45)]
    [Required]
    public string Description { get; set; }

    [InverseProperty(nameof(Employee.EmployeeType))]
    public virtual ICollection<Employee> Employees { get; set; }

  }
}

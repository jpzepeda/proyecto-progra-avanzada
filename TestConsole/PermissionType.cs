using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Packt.Shared
{
  [Table("tipo_permiso")]
  public partial class PermissionType
  {
    public PermissionType()
    {
      Permissions = new HashSet<Permission>();
    }

    [Key]
    [Column("id_tipo_permiso",TypeName = "int")]
    [Required]
    public long PermissionTypeId { get; set; }

    [Column("descripcion",TypeName = "nvarchar (45)")]
    [StringLength(45)]
    [Required]
    public string Description { get; set; }

    [InverseProperty(nameof(Permission.PermissionType))]
    public virtual ICollection<Permission> Permissions { get; set; }

  }
}

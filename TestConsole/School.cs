﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Packt.Shared
{
  public partial class School : DbContext
  {
    public School()
    {
    }

    public School(DbContextOptions<School> options)
        : base(options)
    {
    }

    public virtual DbSet<Calendar> Calendars { get; set; }
    public virtual DbSet<Departament> Departaments { get; set; }
    public virtual DbSet<Employee> Employees { get; set; }
    public virtual DbSet<Permission> Permissions { get; set; }
    public virtual DbSet<Campus> Campuses { get; set; }
    public virtual DbSet<EmployeeType> EmployeeTypes { get; set; }
    public virtual DbSet<PermissionType> PermissionTypes { get; set; }
    public virtual DbSet<Invitation> Invitation { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      if (!optionsBuilder.IsConfigured)
      {
      string path = System.IO.Path.Combine(System.Environment.CurrentDirectory, "../school.db");
      optionsBuilder.UseSqlite($"Filename = {path}");
      }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      /* Using inly data anotations */
      /* OnModelCreatingPartial(modelBuilder); */
    }

  }
}

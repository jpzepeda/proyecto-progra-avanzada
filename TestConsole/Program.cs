﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using static System.Console;
using static Packt.Shared.Permission;

namespace Packt.Shared
{
  class Program
  {
    static void Main(string[] args)
    {
      ListProducts();
    }

    static void ListProducts()
    {
      /* var db = new School(); */
      using (var db = new School())
      {
        WriteLine("{0, -3} {1,-35}",
            "Employee", "Type");
          /* var items = db.Departaments.OrderByDescending(d => d.DepartamentId).ToString(); */
          /* WriteLine(items); */
          foreach (var item in db.Employees.Include(e => e.EmployeeType))
          {
              /* WriteLine(item.ToString()); */
              WriteLine("{0:000} {1,-35}",
              item.Name,item.EmployeeTypeId);
          }
      }
      using (var db = new School())
      {
        WriteLine("{0, -3} {1,-35}",
            "Employee", "Permissions");
          /* var items = db.Departaments.OrderByDescending(d => d.DepartamentId).ToString(); */
          /* WriteLine(items); */
          foreach (var item in db.Employees.Include(e => e.Permissions))
          {
              /* WriteLine(item.ToString()); */
              WriteLine("{0:000} {1,-35}",
              item.Name,item.Permissions.Count);
          }
      }
      using (var db = new School())
      {
        /* WriteLine("{0, -3} {1,-35}", "Permission", "Result"); */
          /* var items = db.Departaments.OrderByDescending(d => d.DepartamentId).ToString(); */
          /* WriteLine(items); */
          foreach (var item in db.Permissions.Include(p => p.PermissionType)
              .Include(p => p.Employee).ThenInclude(e => e.EmployeeType))
          /* foreach (var item in db.Permissions.Include(p => p.Employee)) */
          {
              WriteLine(item.PermissionId.ToString());
              if(item.PermissionId == 4)
              item.PermissionDate = new DateTime(2030,03,06);
              WriteLine(CheckPermision(item,db.Permissions).ToString());
              /* WriteLine("{0:000} {1,-35}", */
              /* item.PermissionId,item.Employee.Name); */
          }
      }
    }

  }
}
